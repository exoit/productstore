using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using WebApp.Database;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Route("api/[controller]")]
    public class ProductsController : Controller
    {
        private readonly PgSqlContext mDbContext;

        public ProductsController(IConfiguration config)
        {
            mDbContext = new PgSqlContext(config);
        }

        [HttpGet]
        public async Task<Dto<List<Product>>> GetProducts()
        {
            try
            {
                var products = await mDbContext.Products.Include(p => p.Category).ToListAsync();

                return new Dto<List<Product>>
                {
                    Data = products
                };
            }
            catch (Exception ex)
            {
                return new Dto<List<Product>>
                {
                    Data = null,
                    Message = ex.Message,
                    Success = false
                };
            }
        }

        [HttpPost]
        public async Task<Dto<object>> AddProduct([FromBody] Product product)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    mDbContext.Products.Add(product);
                    await mDbContext.SaveChangesAsync();

                    return new Dto<object>
                    {
                        Data = await mDbContext.Products.Include(c => c.Category).SingleAsync(o => o.Id == product.Id),
                        Message = "Success saving new product"
                    };
                }
                catch (Exception ex)
                {
                    return new Dto<object>
                    {
                        Data = null,
                        Message = ex.Message,
                        Success = false
                    };
                }
            }

            var sb = new StringBuilder();

            foreach (var item in ModelState)
                sb.AppendLine(string.Join(", ", item.Value.Errors.Select(x => x.ErrorMessage)));

            return new Dto<object>
            {
                Data = null,
                Message = sb.ToString(),
                Success = false
            };
        }
    }
}
