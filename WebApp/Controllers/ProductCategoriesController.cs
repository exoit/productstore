﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using WebApp.Database;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Route("api/[controller]")]
    public class ProductCategoriesController : Controller
    {
        private readonly PgSqlContext mDbContext;

        public ProductCategoriesController(IConfiguration config)
        {
            mDbContext = new PgSqlContext(config);
        }

        [HttpGet]
        public async Task<Dto<List<ProductCategory>>> GetCategories()
        {
            return new Dto<List<ProductCategory>>
            {
                Data = await mDbContext.Categories.ToListAsync()
            };
        }

        [HttpPost]
        public async Task<Dto<string>> AddCategory(string categoryName)
        {
            throw new NotImplementedException();

            return new Dto<string>
            {
                Data = string.Empty
            };
        }

    }
}
