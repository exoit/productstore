﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using WebApp.Validators;

namespace WebApp.Models
{
    [Table("products")]
    public class Product
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("name")]
        [Required, MaxLength(64)]
        public string Name { get; set; }

        [Required, GreaterThanZero]
        [Column("category_id")]
        public int CategoryId { get; set; }

        public virtual ProductCategory Category { get; set; }
    }
}
