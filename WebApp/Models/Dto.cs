﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class Dto<T>
    {
        public T Data { get; set; }

        public string Message { get; set; } = null;

        public bool Success { get; set; } = true;
    }
}
