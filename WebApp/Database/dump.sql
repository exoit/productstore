﻿drop table if exists products;
drop table if exists categories;

create table categories (
	id SERIAL PRIMARY KEY NOT NULL,
	name TEXT NOT NULL
);

create table products (
	id SERIAL PRIMARY KEY NOT NULL,
	name TEXT NOT NULL,
	category_id integer NOT NULL
);
ALTER TABLE products ADD CONSTRAINT product_id_category_id_fkey FOREIGN KEY (category_id) REFERENCES categories (id);

insert into categories (name) values ('Cars'), ('Houses'), ('Computers');
insert into products (name, category_id) values ('Volvo', 1), ('Toyota', 1), ('Intel', 3), ('AMD', 3), ('ARM', 3), ('Hyundai', 1);