﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using WebApp.Models;

namespace WebApp.Database
{
    public class PgSqlContext : DbContext
    {
        public DbSet<Product> Products { get; set; }

        public DbSet<ProductCategory> Categories { get; set; }

        private readonly IConfiguration mConfig;

        public PgSqlContext(IConfiguration config)
        {
            mConfig = config;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var hostname = mConfig.GetValue<string>("Postgresql:Hostname");
            var database = mConfig.GetValue<string>("Postgresql:Database");
            var username = mConfig.GetValue<string>("Postgresql:Username");
            var password = mConfig.GetValue<string>("Postgresql:Password");

            optionsBuilder.UseNpgsql($"Host={hostname};Database={database};Username={username};Password={password}");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>()
                .HasOne(p => p.Category)
                .WithMany(c => c.ProductOf);
        }
    }
}
