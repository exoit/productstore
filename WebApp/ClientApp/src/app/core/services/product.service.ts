import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { Dto } from "../models/Dto.model";
import { Product } from "../models/Product.model";
import { ProductCategory } from "../models/ProductCategory.model";

@Injectable()
export class ProductService {

  private baseUrl : string;

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  getProducts(): Observable<Dto<Array<Product>>> {
    return this.http.get<Dto<Array<Product>>>(this.baseUrl + 'api/Products');
  }

  getProductCategories(): Observable<Dto<Array<ProductCategory>>> {
    return this.http.get<Dto<Array<ProductCategory>>>(this.baseUrl + 'api/ProductCategories');
  }

  addProduct(product: Product): Observable<Dto<Product>> {
    return this.http.post<Dto<Product>>(this.baseUrl + 'api/Products', product);
  }

  //  .subscribe(result => {

    //  if (!result.Success)
    //    console.log(result.Message);

    //  return result.Data;

    //}, error => console.error(error));
  //}


}
