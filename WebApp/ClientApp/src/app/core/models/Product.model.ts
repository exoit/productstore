import { ProductCategory } from "./ProductCategory.model";

export class Product {
  public id: number;
  public name: string;
  public categoryId: number;
  public category: ProductCategory;

  public constructor(name: string, categoryId: number) {
    this.name = name;
    this.categoryId = categoryId;
  }
}
