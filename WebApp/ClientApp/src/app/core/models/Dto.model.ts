
export class Dto<T> {
  public data: T;
  public message: string;
  public success: boolean;
}
