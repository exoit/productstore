import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ProductService } from "../core/services/product.service";
import { Product } from "../core/models/Product.model";
import { ProductCategory } from "../core/models/ProductCategory.model";
import { MatPaginator, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  providers: [ProductService]
})
export class HomeComponent implements  OnInit {

  private products : Array<Product> = [];
  private productCategories: Array<ProductCategory> = [];

  private productDisplayedColumns = ["id", "name", "category"];
  public productDataSource: MatTableDataSource<Product> = new MatTableDataSource<Product>();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private productService: ProductService, public dialog: MatDialog) { }

  ngOnInit() {
    this.productService.getProducts().subscribe(res => {
      if (!res.success)
        console.log(res.message);

      //res.data.forEach(item => {
      //  this.productDataSource.data.push(item);
      //});
      this.productDataSource = new MatTableDataSource<Product>(res.data);
      this.productDataSource.paginator = this.paginator;
    }, error => console.log(error));

    this.productService.getProductCategories().subscribe(res => {
      if (!res.success)
        console.log(res.message);

      this.productCategories = res.data;
    }, error => console.log(error));
  }

  ngAfterViewInit() {
    
  }

  openDialog(): void {
    let name: string = "";
    let category: number = 0;

    let dialogRef = this.dialog.open(DialogAddProduct, {
      width: '290px',
      data: {
        productCategories: this.productCategories,
        name: name,
        category: category
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');

      if (typeof result !== "undefined") {
        var product = new Product(result.name, result.category);

        this.productService.addProduct(product).subscribe(res => {
          if (!res.success) {
            console.log(res.message);
          } else {
            this.productDataSource.data.push(res.data);
            this.productDataSource._updateChangeSubscription();
          }
        }, error => console.log(error));
      }
    });
  }
}

@Component({
  selector: 'dialog-add-product',
  templateUrl: 'dialog-add-product.html',
})
export class DialogAddProduct {

  constructor(
    public dialogRef: MatDialogRef<DialogAddProduct>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

//interface WeatherForecast {
//  dateFormatted: string;
//  temperatureC: number;
//  temperatureF: number;
//  summary: string;
//}

